<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'one4millie' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '5~D8V-=grnU+:)zq=tft<(;HYnj>Ir+)_Z|&#&XR+:PAL2Sc(Tavj;ZyZa*>]q;(' );
define( 'SECURE_AUTH_KEY',  'kaf{]$UXE.dMS$`SNRdEuN4fD69M$1sFwZ&{v~E$0/B5ak`sj0-2riIta{m1%clq' );
define( 'LOGGED_IN_KEY',    'TqY#IKkX0~1P$YSy#0]Lw~bTigD:Mz{B8`6?OD(@TWUfPkE$_?7I4=d!Q8D)|{8q' );
define( 'NONCE_KEY',        '|Jq6QSa`y9Q9G,IP=l=$|-O>+E Ur2JJo(G|^(5_Hu,JT,bFyk QRu7<U}|UU1VL' );
define( 'AUTH_SALT',        'AiriRGggBA,{W_H3k|jbt |C).&/S5o-p,T!oB?1d`kP-RkIwbPHr)Kj0/{.Sm<~' );
define( 'SECURE_AUTH_SALT', 'Mo,Vi_w|mIgpg1qpUM*LQ6sADa]w}^=1>0s@f14`W)%-$&i4GNl|L1Z[z?;^y<d~' );
define( 'LOGGED_IN_SALT',   'dOWy oSwg2yAY3c$+9K5i!|heUO(roE jVRa*1D[P9^j[/Hl.fQkuH;N_9-lvi5i' );
define( 'NONCE_SALT',       'mfZiEame1o2dg,=T}*8)N8PpsN$LKu*CzOd3bwG$XU;1.!B|=>jR`:DN>Q:b+ok9' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
