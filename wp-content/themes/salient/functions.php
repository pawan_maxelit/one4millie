<?php

/**
 * Salient functions and definitions.
 *
 * @package Salient
 * @since 1.0
 */


 /**
  * Define Constants.
  */
define( 'NECTAR_THEME_DIRECTORY', get_template_directory() );
define( 'NECTAR_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/nectar/' );
define( 'NECTAR_THEME_NAME', 'salient' );


if ( ! function_exists( 'get_nectar_theme_version' ) ) {
	function nectar_get_theme_version() {
		return '13.0.6';
	}
}


/**
 * Load text domain.
 */
add_action( 'after_setup_theme', 'nectar_lang_setup' );

if ( ! function_exists( 'nectar_lang_setup' ) ) {
	function nectar_lang_setup() {
		load_theme_textdomain( 'salient', get_template_directory() . '/lang' );
	}
}


/**
 * General WordPress.
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/wp-general.php';


/**
 * Get Salient theme options.
 */
function get_nectar_theme_options() {

	$legacy_options  = get_option( 'salient' );
	$current_options = get_option( 'salient_redux' );

	if ( ! empty( $current_options ) ) {
		return $current_options;
	} elseif ( ! empty( $legacy_options ) ) {
		return $legacy_options;
	} else {
		return $current_options;
	}
}

$nectar_options                    = get_nectar_theme_options();
$nectar_get_template_directory_uri = get_template_directory_uri();

require_once NECTAR_THEME_DIRECTORY . '/includes/class-nectar-theme-manager.php';


/**
 * Register/Enqueue theme assets.
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/icon-collections.php';
require_once NECTAR_THEME_DIRECTORY . '/includes/class-nectar-element-assets.php';
require_once NECTAR_THEME_DIRECTORY . '/includes/class-nectar-element-styles.php';
require_once NECTAR_THEME_DIRECTORY . '/includes/class-nectar-lazy.php';
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/enqueue-scripts.php';
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/enqueue-styles.php';
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/dynamic-styles.php';


/**
 * Salient Plugin notices.
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/plugin-notices/salient-plugin-notices.php';


/**
 * Salient welcome page.
 */
 require_once NECTAR_THEME_DIRECTORY . '/nectar/welcome/welcome-page.php';


/**
 * Theme hooks & actions.
 */
function nectar_hooks_init() {

	require_once NECTAR_THEME_DIRECTORY . '/nectar/hooks/hooks.php';
	require_once NECTAR_THEME_DIRECTORY . '/nectar/hooks/actions.php';

}

add_action( 'after_setup_theme', 'nectar_hooks_init', 10 );


/**
 * Post category meta.
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/meta/category-meta.php';


/**
 * Media and theme image sizes.
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/media.php';


/**
 * Navigation menus
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/assets/functions/wp-menu-custom-items/menu-item-custom-fields.php';
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/nav-menus.php';


/**
 * TGM Plugin inclusion.
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/tgm-plugin-activation/class-tgm-plugin-activation.php';
require_once NECTAR_THEME_DIRECTORY . '/nectar/tgm-plugin-activation/required_plugins.php';


/**
 * WPBakery functionality.
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/wpbakery-init.php';


/**
 * Theme skin specific class and assets.
 */
$nectar_theme_skin    = NectarThemeManager::$skin;
$nectar_header_format = ( ! empty( $nectar_options['header_format'] ) ) ? $nectar_options['header_format'] : 'default';

add_filter( 'body_class', 'nectar_theme_skin_class' );

function nectar_theme_skin_class( $classes ) {
	global $nectar_theme_skin;
	$classes[] = $nectar_theme_skin;
	return $classes;
}


function nectar_theme_skin_css() {
	global $nectar_theme_skin;
	wp_enqueue_style( 'skin-' . $nectar_theme_skin );
}

add_action( 'wp_enqueue_scripts', 'nectar_theme_skin_css' );



/**
 * Search related.
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/search.php';


/**
 * Register Widget areas.
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/widget-related.php';


/**
 * Header navigation helpers.
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/header.php';


/**
 * Blog helpers.
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/blog.php';


/**
 * Page helpers.
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/page.php';
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/footer.php';

/**
 * Theme options panel (Redux).
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/redux-salient.php';


/**
 * WordPress block editor helpers (Gutenberg).
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/gutenberg.php';


/**
 * Admin assets.
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/admin-enqueue.php';


/**
 * Pagination Helpers.
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/pagination.php';


/**
 * Page header.
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/page-header.php';


/**
 * Third party.
 */
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/wpml.php';
require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/woocommerce.php';


/**
 * v10.5 update assist.
 */
 require_once NECTAR_THEME_DIRECTORY . '/nectar/helpers/update-assist.php';

		/* Social Icons*/
		add_action('customize_register','my_customize_register');
function my_customize_register( $wp_customize ){
		$wp_customize->add_section(
			'newton_abbot_socialicons',
			array(
				'title' 		=> __('Social Icons', 'salient'),
				'priority' 		=> 100,
				'capability' 	=> 'edit_theme_options',
				'description' 	=> __('Add social icon Url.', 'salient'),
			)
		);
		$wp_customize->add_setting('facebook_url',
			array(
				'default' 		=> ''
			)
		);
		$wp_customize->add_control(new WP_Customize_Control(
			$wp_customize,
			'facebook_url',
			array(
				'label' 		=> __('Facebook url', 'salient'),
				'section' 		=> 'newton_abbot_socialicons',
				'settings' 		=> 'facebook_url',
				'priority' 		=> 10,
				'type' 			=> 'text',
				'description' 	=> __('Social Icons', 'salient'),
			)
		));

		$wp_customize->add_setting('instagram_url',
			array(
				'default' 		=> ''
			)
		);
		$wp_customize->add_control(new WP_Customize_Control(
			$wp_customize,
			'instagram_url',
			array(
				'label' 		=> __('Instagram url', 'salient'),
				'section' 		=> 'newton_abbot_socialicons',
				'settings' 		=> 'instagram_url',
				'priority' 		=> 10,
				'type' 			=> 'text',
				'description' 	=> __('Social Icons', 'salient'),
			)
		));

		$wp_customize->add_setting('linkedin_url',
			array(
				'default' 		=> ''
			)
		);
		$wp_customize->add_control(new WP_Customize_Control(
			$wp_customize,
			'linkedin_url',
			array(
				'label' 		=> __('Linkedin url', 'salient'),
				'section' 		=> 'newton_abbot_socialicons',
				'settings' 		=> 'linkedin_url',
				'priority' 		=> 10,
				'type' 			=> 'text',
				'description' 	=> __('Social Icons', 'salient'),
			)
		));
	 /** Addesss */
    $wp_customize->add_section(
        'newton_abbot_address',
        array(
            'title' 		=> __('Address', 'salient'),
            'priority' 		=> 100,
            'capability' 	=> 'edit_theme_options',
            'description' 	=> __('Enter the Address.', 'salient'),
        )
    );
    $wp_customize->add_setting('Address',
        array(
            'default' 		=> ''
        )
    );
    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'address',
        array(
            'label' 		=> __('Address', 'salient'),
            'section' 		=> 'newton_abbot_address',
            'settings' 		=> 'Address',
            'priority' 		=> 10,
            'type' 			=> 'text',
            'description' 	=> __('Address', 'salient'),
        )
    ));
	/** contact_No */
    $wp_customize->add_section(
        'newton_abbot_contact_No.',
        array(
            'title' 		=> __('Contact-No.', 'salient'),
            'priority' 		=> 100,
            'capability' 	=> 'edit_theme_options',
            'description' 	=> __('Enter the contact-No..', 'salient'),
        )
    );
    $wp_customize->add_setting('contact-No.',
        array(
            'default' 		=> ''
        )
    );
    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,'contact-No.',
        array(
            'label' 		=> __('contact-No.', 'salient'),
            'section' 		=> 'newton_abbot_contact_No.',
            'settings' 		=> 'contact-No.',
            'priority' 		=> 10,
            'type' 			=> 'text',
            'description' 	=> __('contact-No.', 'salient'),
        )
    ));
	/** email */
    $wp_customize->add_section(
        'newton_abbot_email',
        array(
            'title' 		=> __('Email', 'salient'),
            'priority' 		=> 100,
            'capability' 	=> 'edit_theme_options',
            'description' 	=> __('Enter the Email.', 'salient'),
        )
    );
    $wp_customize->add_setting('email',
        array(
            'default' 		=> ''
        )
    );
    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,'email',
        array(
            'label' 		=> __('email', 'salient'),
            'section' 		=> 'newton_abbot_email',
            'settings' 		=> 'email',
            'priority' 		=> 10,
            'type' 			=> 'text',
            'description' 	=> __('email', 'salient'),
        )
    ));
	}